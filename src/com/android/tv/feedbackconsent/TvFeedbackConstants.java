/*
 * Copyright (C) 2023 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.tv.feedbackconsent;

/**
 * Constants for Feedback consent results.
 */
public final class TvFeedbackConstants {

    public static final String BUGREPORT_CONSENT = "BUGREPORT_CONSENT";
    public static final String BUGREPORT_REQUESTED = "BUGREPORT_REQUESTED";
    public static final String CONSENT_RECEIVER = "CONSENT_RECEIVER";
    public static final String CANCEL_REQUEST = "CANCEL_REQUEST";
    public static final int RESULT_CODE_OK = 0;
    public static final String SYSTEM_LOGS_CONSENT = "SYSTEM_LOGS_CONSENT";
    public static final String SYSTEM_LOGS_KEY = "SYSTEM_LOGS_KEY";
    public static final String SYSTEM_LOGS_REQUESTED = "SYSTEM_LOGS_REQUESTED";

}