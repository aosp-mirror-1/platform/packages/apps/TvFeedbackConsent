//
// Copyright (C) 2023 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

prebuilt_etc {
    name: "privapp_permissions_com.android.tv.feedbackconsent",
    system_ext_specific: true,
    sub_dir: "permissions",
    src: "privapp-permissions-com.android.tv.feedbackconsent.xml",
    filename_from_src: true,
}

prebuilt_etc {
    name: "config-com.android.tv.feedbackconsent",
    system_ext_specific: true,
    relative_install_path: "sysconfig",
    src: "config-com.android.tv.feedbackconsent.xml",
    filename_from_src: true,
}

android_library {
    name: "TvFeedbackConsent-core",
    srcs: [
        "src/**/*.java",
        "src/**/*.kt",
        "src/**/I*.aidl",
    ],
    resource_dirs: [
        "res",
    ],
    static_libs: [
        "androidx.leanback_leanback-preference",
        "androidx.leanback_leanback",
        "androidx.recyclerview_recyclerview",
    ],
    manifest: "AndroidManifest.xml",
}

android_app {
    name: "TvFeedbackConsent",
    resource_dirs: [],
    static_libs: [
        "TvFeedbackConsent-core",
    ],

    platform_apis: true,
    system_ext_specific: true,
    certificate: "platform",
    privileged: true,
    required: [
        "privapp_permissions_com.android.tv.feedbackconsent",
        "config-com.android.tv.feedbackconsent",
    ],
    optimize: {
        optimize: true,
        optimized_shrink_resources: true,
    },
}
